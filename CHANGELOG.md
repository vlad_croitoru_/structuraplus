# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.0.3] - 2024-04-30

### Added

- Enhanced code documentation
- This Changelog
- .gitlab-ci.yml with Gradle build and checkstyle linter.

## [0.0.2] - 2024-02-29

### Added

- Crafting recipes and textures for all items below.
- Steel Armor (helmet, chestplate, leggings, boots).
- Steel tools (sword, pickaxe, axe, shovel, hoe).
- Steel plates.
- Pure iron ingots, nuggets and blocks. 
- Carbon ore generation.

## [0.0.1] - 2024-02-09

### Added

- Crafting recipes and textures for all items below.
- Steel nuggets and carbon nuggets.
- Carbon ores and Deepslate carbon ores.
- Carbon blocks and Steel blocks.
- Steel and carbon ingots.
- Project LICENSE (GPLv3).
- README.md template.

### Changed

- Update Minecraft version from 1.18.1 to 1.18.2.

[0.0.3]: https://gitlab.com/vlad_croitoru_/structuraplus/-/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/vlad_croitoru_/structuraplus/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/vlad_croitoru_/structuraplus/-/commits/v0.0.1