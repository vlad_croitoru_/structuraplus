<a name="readme-top"></a>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/vlad_croitoru_/structuraplus.git">
    <img src="ModIcon.png" alt="Logo" width="auto" height="150">
  </a>

<h3 align="center">StructuraPlus</h3>

  <p align="center">
    A Minecraft adventure mod
    <br />
    <a href="https://gitlab.com/vlad_croitoru_/structuraplus.git"><strong>Explore the documentation »</strong></a>
    <br />
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about">About</a>
      <ul>
        <li><a href="#build-with">Built with</a></li>
      </ul>
    </li>
    <li>
      <a href="#installation">Installation</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT -->

## About

<p>
StructuraPlus introduces new items, blocks, tools, and armor sets, adding to the Minecraft gameplay experience. In the future, I plan to add structures to StructuraPlus. These new additions will complement the existing items, blocks, tools, and armor sets, providing players with more options for exploration, adventure and gameplay.
</p>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Built with:

* [![Java][Java.com]][Java-url]
* [![Gradle][Gradle.org]][Gradle-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Installation

### Prerequisites

1. <a href="https://git-scm.com/downloads"><strong>Git</strong></a>

2. <a href="https://gradle.org/install/"><strong>Gradle</strong></a>

3. <a href="https://www.oracle.com/java/technologies/downloads/"><strong>Java 17 JDK</strong></a>

### Steps

1. Clone the repository:

```sh
   git clone https://gitlab.com/vlad_croitoru_/structuraplus.git
   ```

2. Open the project in an IDE. I recommend using <a href="https://www.oracle.com/java/technologies/downloads/"><strong>IntelliJ IDEA Community</strong></a>.

3. Run the Minecraft Client:

```sh
  gradle runClient
   ```

<!-- ROADMAP -->

## Roadmap

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a full list of proposed features (
and known issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## License

StructuraPlus is licensed under the GPLv3 license. Please
see [`LICENSE.txt`](https://gitlab.com/vlad_croitoru_/structuraplus/-/blob/main/LICENSE) for more info.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->

## Contact

Project Link: https://gitlab.com/vlad_croitoru_/structuraplus.git

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[Java.com]: https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white

[Java-url]: https://www.java.com/en/

[Gradle.org]: https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white

[Gradle-url]: https://gradle.org/