package net.coco.structuraplus.world.gen;

/**
 * The ModWorldGen class manages world generation features for the StructuraPlus mod.
 * It includes a method to generate custom world generation content.
 */
public class ModWorldGen {

    /**
     * Generates custom world generation content, including ores and other features.
     */
    public static void generateModWorldGen() {
        ModOreGeneration.generateOres(); // Generates custom ores using the ModOreGeneration class
    }
}
