package net.coco.structuraplus.world.gen;

import net.coco.structuraplus.world.feature.ModPlacedFeatures;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.world.gen.GenerationStep;

/**
 * The ModOreGeneration class manages custom ore generation for the StructuraPlus mod.
 * It includes a method to generate ores in the game world.
 */
public class ModOreGeneration {

    /**
     * Generates custom ores in the game world by adding a placed feature to specific biomes.
     */
    public static void generateOres() {
        BiomeModifications.addFeature(
                BiomeSelectors.foundInOverworld(), // Selects biomes in the overworld
                GenerationStep.Feature.UNDERGROUND_ORES, // Specifies the generation step
                ModPlacedFeatures.CARBON_ORE_PLACED.getKey().get() // Gets the placed feature for carbon ore generation
        );
    }
}
