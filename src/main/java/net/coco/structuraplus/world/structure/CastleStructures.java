package net.coco.structuraplus.world.structure;

import com.mojang.serialization.Codec;
import net.minecraft.structure.StructureGeneratorFactory;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.gen.feature.StructurePoolFeatureConfig;

/**
 * The CastleStructures class represents a custom structure feature for castle generation in Minecraft.
 * It extends the StructureFeature class and provides configuration and generation logic for castle structures.
 */
public class CastleStructures extends StructureFeature<StructurePoolFeatureConfig> {

    /**
     * Constructs a new CastleStructures object with the specified configuration codec and pieces generator.
     *
     * @param configCodec     The codec for the structure pool feature configuration.
     * @param piecesGenerator The generator factory for generating structure pieces.
     */
    public CastleStructures(Codec<StructurePoolFeatureConfig> configCodec, StructureGeneratorFactory<StructurePoolFeatureConfig> piecesGenerator) {
        super(configCodec, piecesGenerator);
    }
}
