package net.coco.structuraplus.world.feature;

import net.coco.structuraplus.StructuraPlus;
import net.coco.structuraplus.block.ModBlocks;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.world.gen.feature.*;

import java.util.List;

/**
 * The ModConfiguredFeatures class manages custom configured features for the StructuraPlus mod.
 * It includes configuration for generating custom ores in the game world.
 */
public class ModConfiguredFeatures {

    /**
     * List of targets for generating carbon ores in the overworld.
     */
    public static final List<OreFeatureConfig.Target> OVERWORLD_CARBON_ORES = List.of(
            OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES,
                    ModBlocks.CARBON_ORE.getDefaultState()),
            OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES,
                    ModBlocks.DEEPSLATE_CARBON_ORE.getDefaultState())
    );

    /**
     * Configured feature for generating carbon ore deposits in the game world.
     */
    public static final RegistryEntry<ConfiguredFeature<OreFeatureConfig, ?>> CARBON_ORE =
            ConfiguredFeatures.register("carbon_ore", Feature.ORE,
                    new OreFeatureConfig(OVERWORLD_CARBON_ORES, 6));

    /**
     * Method to register the configured features for the mod.
     */
    public static void registerConfiguredFeatures() {
        StructuraPlus.LOGGER.info("Registering ModConfiguredFeatures for" + StructuraPlus.MOD_ID);
    }
}
