package net.coco.structuraplus.world.feature;

import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.feature.PlacedFeature;
import net.minecraft.world.gen.feature.PlacedFeatures;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.world.gen.placementmodifier.HeightRangePlacementModifier;

/**
 * The ModPlacedFeatures class manages custom placed features for the StructuraPlus mod.
 * It includes registration of a placed feature for generating carbon ore deposits.
 */
public class ModPlacedFeatures {

    /**
     * RegistryEntry for a custom placed feature that generates carbon ore deposits.
     */
    public static final RegistryEntry<PlacedFeature> CARBON_ORE_PLACED = PlacedFeatures.register(
            "carbon_ore_placed",
            ModConfiguredFeatures.CARBON_ORE,
            ModOreFeatures.modifiersWithCount(5,
                    HeightRangePlacementModifier.trapezoid(YOffset.aboveBottom(-80), YOffset.aboveBottom(80))));

}