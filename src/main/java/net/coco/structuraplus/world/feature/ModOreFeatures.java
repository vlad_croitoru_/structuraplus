package net.coco.structuraplus.world.feature;

import net.minecraft.world.gen.placementmodifier.*;

import java.util.List;

/**
 * The ModOreFeatures class provides utility methods for managing placement modifiers related to ore generation.
 */
public class ModOreFeatures {

    /**
     * Constructs a list of placement modifiers based on count and height modifiers.
     *
     * @param countModifier  The count modifier to be included in the list.
     * @param heightModifier The height modifier to be included in the list.
     * @return A list of placement modifiers.
     */
    public static List<PlacementModifier> modifiers(PlacementModifier countModifier, PlacementModifier heightModifier) {
        return List.of(countModifier, SquarePlacementModifier.of(), heightModifier, BiomePlacementModifier.of());
    }

    /**
     * Constructs a list of placement modifiers with a count modifier and height modifier.
     *
     * @param count          The count value for the count modifier.
     * @param heightModifier The height modifier to be included in the list.
     * @return A list of placement modifiers.
     */
    public static List<PlacementModifier> modifiersWithCount(int count, PlacementModifier heightModifier) {
        return modifiers(CountPlacementModifier.of(count), heightModifier);
    }

    /**
     * Constructs a list of placement modifiers with a rarity filter modifier and height modifier.
     *
     * @param chance         The chance value for the rarity filter modifier.
     * @param heightModifier The height modifier to be included in the list.
     * @return A list of placement modifiers.
     */
    public static List<PlacementModifier> modifiersWithRarity(int chance, PlacementModifier heightModifier) {
        return modifiers(RarityFilterPlacementModifier.of(chance), heightModifier);
    }
}