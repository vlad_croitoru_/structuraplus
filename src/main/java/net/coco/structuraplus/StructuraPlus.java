package net.coco.structuraplus;

import net.coco.structuraplus.block.ModBlocks;
import net.coco.structuraplus.item.ModItems;
import net.coco.structuraplus.world.feature.ModConfiguredFeatures;
import net.coco.structuraplus.world.gen.ModWorldGen;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The StructuraPlus class serves as the main entry point for the StructuraPlus mod.
 * It implements the ModInitializer interface to initialize the mod during the game's startup.
 */
public class StructuraPlus implements ModInitializer {

    public static final String MOD_ID = "structuraplus";


    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    /**
     * This method is called during the game's initialization phase.
     * It registers mod items, blocks, configured features,
     * and generates world generation for the mod.
     */
    @Override
    public void onInitialize() {
        // Register mod items, blocks, and configured features
        ModItems.registerModItems();
        ModBlocks.registerModBlocks();
        ModConfiguredFeatures.registerConfiguredFeatures();

        // Generate mod-specific world generation structures
        ModWorldGen.generateModWorldGen();

        // Log initialization message
        LOGGER.info("StructuraPlus initialized");
    }
}
