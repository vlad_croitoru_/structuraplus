package net.coco.structuraplus.block;

import net.coco.structuraplus.StructuraPlus;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * The ModBlocks class is responsible for registering and managing custom blocks added by the StructuraPlus mod.
 * It includes several static final Block objects representing different blocks in the mod.
 */
public class ModBlocks {


    public static final Block STEEL_BLOCK = registerBlock("steel_block", new Block(FabricBlockSettings
            .of(Material.METAL).strength(6f).requiresTool()));

    public static final Block PURE_IRON_BLOCK = registerBlock("pure_iron_block", new Block(FabricBlockSettings
            .of(Material.METAL).strength(6f).requiresTool()));

    public static final Block CARBON_BLOCK = registerBlock("carbon_block", new Block(FabricBlockSettings
            .of(Material.METAL).strength(6f).requiresTool()));

    public static final Block CARBON_ORE = registerBlock("carbon_ore",
            new Block(FabricBlockSettings.of(Material.STONE).strength(4.5f).requiresTool()));

    public static final Block DEEPSLATE_CARBON_ORE = registerBlock("deepslate_carbon_ore",
            new Block(FabricBlockSettings.of(Material.STONE).strength(4.0f).requiresTool()));


    public static final Block RAW_CARBON_BLOCK = registerBlock("raw_carbon_block", new Block(FabricBlockSettings
            .of(Material.METAL).strength(4.0f).requiresTool()));

    /**
     * Helper method to register a block and its corresponding item.
     *
     * @param name  The name of the block.
     * @param block The Block object to register.
     * @return The registered Block object.
     */
    private static Block registerBlock(String name, Block block) {
        registerBlockItem(name, block);
        return Registry.register(Registry.BLOCK, new Identifier(StructuraPlus.MOD_ID, name), block);
    }

    /**
     * Helper method to register a BlockItem for a given block.
     *
     * @param name  The name of the block.
     * @param block The Block object to create a BlockItem for.
     */
    private static void registerBlockItem(String name, Block block) {
        Registry.register(Registry.ITEM, new Identifier(StructuraPlus.MOD_ID, name), new BlockItem(block,
                new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS)));
    }

    /**
     * Method to register all custom blocks in the mod.
     */
    public static void registerModBlocks() {
        StructuraPlus.LOGGER.info("Registering Mod Blocks for " + StructuraPlus.MOD_ID);
    }
}