package net.coco.structuraplus;

import net.fabricmc.api.ClientModInitializer;

/**
 * The StructuraPlusClient class serves as the client-side entry point for the StructuraPlus mod.
 * It implements the ClientModInitializer interface to initialize client-specific features during the game's startup.
 */
public class StructuraPlusClient implements ClientModInitializer {

    /**
     * This method is called during the game's client-side initialization phase.
     * It is used to initialize client-specific features such as rendering, GUI, and client-side logic.
     */
    @Override
    public void onInitializeClient() {
        // Add client-side initialization code here
    }
}
