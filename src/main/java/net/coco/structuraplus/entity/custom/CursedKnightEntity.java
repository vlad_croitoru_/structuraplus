package net.coco.structuraplus.entity.custom;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.world.World;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
/**
 * CursedKnightEntity class which extends the `MobEntity` class and implements the `IAnimatable` interface.
 * This class represents a custom entity, the Cursed Knight, for a Minecraft mod.
 */
public class CursedKnightEntity extends MobEntity implements IAnimatable {

    /**
     * Constructor for the `CursedKnightEntity` class.
     *
     * @param entityType This is the type of the entity. It should extend `MobEntity`.
     * @param world This is the world in which the entity exists.
     */
    protected CursedKnightEntity(EntityType<? extends MobEntity> entityType, World world) {
        super(entityType, world);
    }

    /**
     * This method is used to register controllers for the entity's animations.
     *
     * @param animationData This is the data for the entity's animations.
     */
    @Override
    public void registerControllers(AnimationData animationData) {

    }

    /**
     * This method is used to get the `AnimationFactory` for the entity.
     *
     * @return AnimationFactory This returns the animation factory for the entity.
     */
    @Override
    public AnimationFactory getFactory() {
        return null;
    }
}
