package net.coco.structuraplus.item;

import net.fabricmc.yarn.constants.MiningLevels;
import net.minecraft.item.ToolMaterial;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Lazy;

import java.util.function.Supplier;

/**
 * The `ModToolMaterials` enum implements the `ToolMaterial` interface and represents different types of materials that can be used to craft tools in the Minecraft mod.
 * Each enum constant represents a different type of tool material, with its own properties such as mining level, durability, mining speed, attack damage, enchantability, and repair ingredient.
 */
public enum ModToolMaterials implements ToolMaterial {

    /**
     * The `STEEL` constant represents a tool material made of steel.
     * It has the mining level of diamond, a durability of 1861, a mining speed of 8.5f,
     * an attack damage of 3.5f, an enchantability of 13, and its repair ingredient is a steel ingot.
     */
    STEEL(MiningLevels.DIAMOND, 1861, 8.5f, 3.5f, 13, () -> Ingredient.ofItems(ModItems.STEEL_INGOT));

    private final int miningLevel;
    private final int itemDurability;
    private final float miningSpeed;
    private final float attackDamage;
    private final int enchantability;
    private final Lazy<Ingredient> repairIngredient;

    /**
     * This is the constructor for the `ModToolMaterials` enum.
     *
     * @param miningLevel      This is the mining level of the tool material.
     * @param itemDurability   This is the durability of the tool material.
     * @param miningSpeed      This is the mining speed of the tool material.
     * @param attackDamage     This is the attack damage of the tool material.
     * @param enchantability   This is the enchantability of the tool material.
     * @param repairIngredient This is the ingredient used to repair tools made of this material.
     */
    ModToolMaterials(int miningLevel, int itemDurability, float miningSpeed, float attackDamage, int enchantability, Supplier<Ingredient> repairIngredient) {
        this.miningLevel = miningLevel;
        this.itemDurability = itemDurability;
        this.miningSpeed = miningSpeed;
        this.attackDamage = attackDamage;
        this.enchantability = enchantability;
        this.repairIngredient = new Lazy<>(repairIngredient);
    }

    /**
     * This method returns the durability of the tool material.
     *
     * @return int This returns the durability of the tool material.
     */
    @Override
    public int getDurability() {
        return this.itemDurability;
    }

    /**
     * This method returns the mining speed multiplier of the tool material.
     *
     * @return float This returns the mining speed multiplier of the tool material.
     */
    @Override
    public float getMiningSpeedMultiplier() {
        return this.miningSpeed;
    }

    /**
     * This method returns the attack damage of the tool material.
     *
     * @return float This returns the attack damage of the tool material.
     */
    @Override
    public float getAttackDamage() {
        return this.attackDamage;
    }

    /**
     * This method returns the mining level of the tool material.
     *
     * @return int This returns the mining level of the tool material.
     */
    @Override
    public int getMiningLevel() {
        return this.miningLevel;
    }

    /**
     * This method returns the enchantability of the tool material.
     *
     * @return int This returns the enchantability of the tool material.
     */
    @Override
    public int getEnchantability() {
        return this.enchantability;
    }

    /**
     * This method returns the repair ingredient of the tool material.
     *
     * @return Ingredient This returns the repair ingredient of the tool material.
     */
    @Override
    public Ingredient getRepairIngredient() {
        return this.repairIngredient.get();
    }
}
