package net.coco.structuraplus.item;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.Items;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Lazy;

import java.util.function.Supplier;

/**
 * This class represents an armor material in Minecraft.
 * It defines the properties of the armor material such as durability, protection amount, enchantability, equip sound, repair ingredient, name, toughness, and knockback resistance.
 */
public enum ModArmorMaterials implements ArmorMaterial {

    /**
     * The `STEEL` constant represents an armor material made of steel.
     * It has a durability multiplier of 35, protection amounts of {3, 6, 8, 3},
     * an enchantability of 10, an equip sound of `SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND`,
     * a toughness of 2.5f, a knockback resistance of 0.05f, and its repair ingredient is a steel ingot.
     */
    STEEL("steel", 35, new int[]{3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.5f, 0.05f, () -> Ingredient.ofItems(ModItems.STEEL_INGOT));

    private static final int[] BASE_DURABILITY;
    private final String name;
    private final int durabilityMultiplier;
    private final int[] protectionAmounts;
    private final int enchantability;
    private final SoundEvent equipSound;
    private final float toughness;
    private final float knockbackResistance;
    private final Lazy<Ingredient> repairIngredientSupplier;

    /**
     * Constructs an ArmorMaterial with the given properties.
     *
     * @param name                     The name of the armor material.
     * @param durabilityMultiplier     The multiplier that is used to calculate the durability of the armor material for a given equipment slot.
     * @param protectionAmounts        An array containing the protection amounts for each equipment slot.
     * @param enchantability           The enchantability of the armor material.
     * @param equipSound               The sound event that is played when the armor is equipped.
     * @param toughness                The toughness of the armor material.
     * @param knockbackResistance      The knockback resistance provided by the armor material.
     * @param repairIngredientSupplier A supplier providing the ingredient used to repair the armor material.
     */
    ModArmorMaterials(String name, int durabilityMultiplier, int[] protectionAmounts, int enchantability, SoundEvent equipSound, float toughness, float knockbackResistance, Supplier<Ingredient> repairIngredientSupplier) {
        this.name = name;
        this.durabilityMultiplier = durabilityMultiplier;
        this.protectionAmounts = protectionAmounts;
        this.enchantability = enchantability;
        this.equipSound = equipSound;
        this.toughness = toughness;
        this.knockbackResistance = knockbackResistance;
        this.repairIngredientSupplier = new Lazy<Ingredient>(repairIngredientSupplier);
    }


    static {
        BASE_DURABILITY = new int[]{13, 15, 16, 11};
    }

    /**
     * Returns the durability of the armor material for a given equipment slot.
     * The durability is calculated by multiplying the base durability of the slot by a durability multiplier.
     *
     * @param slot The equipment slot for which the durability is to be returned.
     * @return The durability of the armor material for the given equipment slot.
     */
    @Override
    public int getDurability(EquipmentSlot slot) {
        return BASE_DURABILITY[slot.getEntitySlotId()] * this.durabilityMultiplier;
    }

    /**
     * Returns the protection amount provided by the armor material for a given equipment slot.
     *
     * @param slot The equipment slot for which the protection amount is to be returned.
     * @return The protection amount of the armor material for the given equipment slot.
     */
    @Override
    public int getProtectionAmount(EquipmentSlot slot) {
        return this.protectionAmounts[slot.getEntitySlotId()];
    }

    /**
     * Returns the enchantability of the armor material.
     * Enchantability is the property that determines how good the armor material is at receiving enchantments.
     *
     * @return The enchantability of the armor material.
     */
    @Override
    public int getEnchantability() {
        return this.enchantability;
    }

    /**
     * Returns the sound event that is played when the armor is equipped.
     *
     * @return The equip sound of the armor material.
     */
    @Override
    public SoundEvent getEquipSound() {
        return this.equipSound;
    }

    /**
     * Returns the ingredient used to repair the armor material.
     *
     * @return The repair ingredient of the armor material.
     */
    @Override
    public Ingredient getRepairIngredient() {
        return this.repairIngredientSupplier.get();
    }

    /**
     * Returns the name of the armor material.
     *
     * @return The name of the armor material.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Returns the toughness of the armor material.
     * Toughness is a property that reduces the damage taken from high-damage attacks.
     *
     * @return The toughness of the armor material.
     */
    @Override
    public float getToughness() {
        return this.toughness;
    }

    /**
     * Returns the knockback resistance provided by the armor material.
     * Knockback resistance is a property that reduces the distance the player is knocked back when hit.
     *
     * @return The knockback resistance of the armor material.
     */
    @Override
    public float getKnockbackResistance() {
        return this.knockbackResistance;
    }

}
