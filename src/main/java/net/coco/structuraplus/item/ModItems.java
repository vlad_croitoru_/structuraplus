package net.coco.structuraplus.item;

import net.coco.structuraplus.StructuraPlus;
import net.coco.structuraplus.item.custom.ModAxeItem;
import net.coco.structuraplus.item.custom.ModHoeItem;
import net.coco.structuraplus.item.custom.ModPickaxeItem;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * Class containing all custom items.
 */
public class ModItems {

    //ARMOR
    public static final Item STEEL_HELMET = registerItem("steel_helmet", new ArmorItem(ModArmorMaterials.STEEL, EquipmentSlot.HEAD, new FabricItemSettings().group(ItemGroup.COMBAT)));

    public static final Item STEEL_CHESTPLATE = registerItem("steel_chestplate", new ArmorItem(ModArmorMaterials.STEEL, EquipmentSlot.CHEST, new FabricItemSettings().group(ItemGroup.COMBAT)));

    public static final Item STEEL_LEGGINGS = registerItem("steel_leggings", new ArmorItem(ModArmorMaterials.STEEL, EquipmentSlot.LEGS, new FabricItemSettings().group(ItemGroup.COMBAT)));

    public static final Item STEEL_BOOTS = registerItem("steel_boots", new ArmorItem(ModArmorMaterials.STEEL, EquipmentSlot.FEET, new FabricItemSettings().group(ItemGroup.COMBAT)));

    //TOOLS
    public static Item STEEL_SWORD = registerItem("steel_sword", new SwordItem(ModToolMaterials.STEEL, 3, -2.4f, new FabricItemSettings().group(ItemGroup.COMBAT)));

    public static Item STEEL_SHOVEL = registerItem("steel_shovel", new ShovelItem(ModToolMaterials.STEEL, 1.5f, -3f, new FabricItemSettings().group(ItemGroup.TOOLS)));

    public static Item STEEL_AXE = registerItem("steel_axe", new ModAxeItem(ModToolMaterials.STEEL, 5, -3f, new FabricItemSettings().group(ItemGroup.TOOLS)));

    public static Item STEEL_HOE = registerItem("steel_hoe", new ModHoeItem(ModToolMaterials.STEEL, -3, 0f, new FabricItemSettings().group(ItemGroup.TOOLS)));

    public static Item STEEL_PICKAXE = registerItem("steel_pickaxe", new ModPickaxeItem(ModToolMaterials.STEEL, 1, -2.8f, new FabricItemSettings().group(ItemGroup.TOOLS)));

    // MATERIAL ITEMS
    public static final Item STEEL_INGOT = registerItem("steel_ingot", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item STEEL_NUGGET = registerItem("steel_nugget", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item STEEL_PLATE = registerItem("steel_plate", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item PURE_IRON_INGOT = registerItem("pure_iron_ingot", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item PURE_IRON_NUGGET = registerItem("pure_iron_nugget", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item CARBON_INGOT = registerItem("carbon_ingot", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item RAW_CARBON = registerItem("raw_carbon", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item CARBON_NUGGET = registerItem("carbon_nugget", new Item(new FabricItemSettings().group(ItemGroup.MISC)));

    /**
     * Registers a custom item into the Minecraft registry.
     *
     * @param name The name of the item.
     * @param item The item instance.
     * @return The registered item.
     */
    private static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier(StructuraPlus.MOD_ID, name), item);
    }

    /**
     * Registers all custom items of the mod.
     */
    public static void registerModItems() {
        StructuraPlus.LOGGER.info("Registering Mod Items for" + StructuraPlus.MOD_ID);
    }
}
