package net.coco.structuraplus.item.custom;

import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;

/**
 * The ModPickaxeItem class represents a custom pickaxe item added by the StructuraPlus mod.
 * It extends the PickaxeItem class from Minecraft and provides customization options for pickaxe items.
 */
public class ModPickaxeItem extends PickaxeItem {

    /**
     * Constructs a new ModPickaxeItem with the specified parameters.
     *
     * @param material     The ToolMaterial representing the material of the pickaxe.
     * @param attackDamage The base attack damage of the pickaxe.
     * @param attackSpeed  The attack speed of the pickaxe.
     * @param settings     The settings for the pickaxe item.
     */
    public ModPickaxeItem(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}