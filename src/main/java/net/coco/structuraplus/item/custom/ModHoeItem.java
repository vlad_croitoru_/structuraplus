package net.coco.structuraplus.item.custom;

import net.minecraft.item.HoeItem;
import net.minecraft.item.ToolMaterial;

/**
 * The ModHoeItem class represents a custom hoe item added by the StructuraPlus mod.
 * It extends the HoeItem class from Minecraft and provides customization options for hoe items.
 */
public class ModHoeItem extends HoeItem {

    /**
     * Constructs a new ModHoeItem with the specified parameters.
     *
     * @param material     The ToolMaterial representing the material of the hoe.
     * @param attackDamage The base attack damage of the hoe.
     * @param attackSpeed  The attack speed of the hoe.
     * @param settings     The settings for the hoe item.
     */
    public ModHoeItem(ToolMaterial material, int attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}