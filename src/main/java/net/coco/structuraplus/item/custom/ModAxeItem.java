package net.coco.structuraplus.item.custom;

import net.minecraft.item.AxeItem;
import net.minecraft.item.ToolMaterial;

/**
 * The ModAxeItem class represents a custom axe item added by the StructuraPlus mod.
 * It extends the AxeItem class from Minecraft and provides customization options for axe items.
 */
public class ModAxeItem extends AxeItem {

    /**
     * Constructs a new ModAxeItem with the specified parameters.
     *
     * @param material     The ToolMaterial representing the material of the axe.
     * @param attackDamage The base attack damage of the axe.
     * @param attackSpeed  The attack speed of the axe.
     * @param settings     The settings for the axe item.
     */
    public ModAxeItem(ToolMaterial material, float attackDamage, float attackSpeed, Settings settings) {
        super(material, attackDamage, attackSpeed, settings);
    }
}