package net.coco.structuraplus.mixin;

import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.feature.StructureFeature;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

/**
 * A Mixin interface for the StructureFeature class.
 * It provides an invoker for the private method 'register' in the StructureFeature class.
 */
@Mixin(StructureFeature.class)
public interface StructureFeatureAccessor {

    /**
     * Invokes the 'register' method of the StructureFeature class.
     *
     * @param name             The registry name of the structure feature.
     * @param structureFeature The instance of the structure feature to be registered.
     * @param step             The generation step at which the structure feature is to be generated.
     * @return The registered structure feature.
     * @throws UnsupportedOperationException If the method is called directly (it should be called through a Mixin).
     */
    @Invoker
    static <F extends StructureFeature<?>> F callRegister(String name, F structureFeature, GenerationStep.Feature step) {
        throw new UnsupportedOperationException();
    }
}
